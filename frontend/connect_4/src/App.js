import {BrowserRouter as Router, Route, Routes, Navigate} from "react-router-dom"
import LoginPage from "./components/LoginPage"
import RegisterPage from "./components/RegisterPage"
import {useState} from "react"

import './App.css';
import {socket, SocketContext} from "./context/socket"
import GamePage from "./components/GamePage";


function App() {

  const [state, setState] = useState({user_id : undefined, loggedIn: false, name : undefined})
  
  return (
    <SocketContext.Provider value={socket} >
    <Router>
      <Routes>
        
          {!state.loggedIn && 
          <>
            <Route path="/" exact element={<LoginPage setState={setState}/>}/> 
            <Route path="/game" element={<Navigate replace to={"/"}/>}/> 
            <Route path="/register" element={<RegisterPage setState={setState} />} />
          </>}
          {state.loggedIn && <Route path="/game" element={<GamePage loggedIn={state.loggedIn} setState={setState} username={state.name}/>}/>}
      </Routes>
    </Router>
    </SocketContext.Provider>
  );
}

export default App;
