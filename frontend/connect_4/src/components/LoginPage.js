import { useState } from "react";
import {useNavigate, Link} from "react-router-dom"
import "./login.css"

export default function LoginPage({setState}) {
    // State zum Verwalten des Benutzerinputs des Felds Benutzername
    const [benutzername, setBenutzername] = useState('');
    // State zum Input Passwort
    const [passwort, setPasswort] = useState('');
    
    const [error, setError] = useState("");
    
    const navigate = useNavigate();

    const handleSubmit =async(setAppState) => {
        try 
        {   
            let res = await fetch("http://localhost:8001/user/login", {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({name : benutzername, passwort : passwort})
        })
        
            let data = await res.json()
            if(res.ok && data != null){
                setAppState({loggedIn : true, user_id: data.user.id, name: data.user.name})
                console.log(data.jwt)
                navigate("/game", {replace: true})  
            }
            else {
                setError("Bitte Prüfen sie ihre angaben")
            }
        }
        catch 
        {
            setError("Internal Server error!!!")
        }
       
        
    }
    
    return (
        <form className="d-flex flex-column w-25 mx-auto my-3 border border-1 rounded-2 p-3"
            onSubmit={(e) => {
                e.preventDefault()
                handleSubmit(setState)
            }}>
            <fieldset className='d-flex flex-column align-self-center'>
                { error.length > 1 && <div className="alert alert-danger" role="alert">{error}</div>}
                <legend>Connect 4</legend>
                <div className="form-group">
                    <label className="form-label mt-3 ">Benutzername:</label>

                    <input className="form-control"
                        value={benutzername}
                        onChange={e => setBenutzername(e.target.value)} />

                </div>
                <div className="form-group">
                    <label className="form-label mt-3 ">Passwort:</label>

                    <input className="form-control"
                        type="password"
                        value={passwort}
                        onChange={e => setPasswort(e.target.value)} />
                </div>
            </fieldset>
            <Link to="/register" >Noch kein Konto?</Link>
            <input type="submit" className="login-btn my-3 w-25 btn  align-self-center" value="Login" />
            
    </form>)
}