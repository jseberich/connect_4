import { useContext, useEffect, useState } from "react"
import { Navigate } from "react-router-dom";
import { SocketContext } from "../context/socket";
import "./game.css"
const GamePage = ({ username, loggedIn, setState }) => {

    const socket = useContext(SocketContext)
    let [gameState, setGameState] = useState({
        running: false,
        room: undefined,
        board: []
    })
    let [msg, setMsg] = useState("")

    const handleMatch = () => {
        socket.emit("match")
    }

    const turn = (col) => {
        socket.emit("turn", { data: col })
    }

    const logout = () => {
        setState({user_id : undefined, loggedIn: false, name : undefined})
    }
    const leave_room = () => {
        setMsg("")
        socket.emit("leave_room")
        setGameState({ running: false, room: undefined, board: [] })
    }

    const getColor = (square) => {
        switch (square) {
            case 0:
                return "white"
            case 1:
                return "yellow"
            case 2:
                return "red"
            default:
                break;
        }
    }
    useEffect(() => {
        socket.send(username)
        socket.on("match", data => {
            setMsg("")
            setGameState({ running: true, room: data.room, board: data.board })
        })
        socket.on("leave_room", () => { console.log("U won. The other Player left the room") })
        socket.on("new_board", data => {
            setGameState(state => ({ ...state, board: data.board }))
        })
        socket.on("end", data => {
            if (data.winner == username) {
                setMsg("Du hast gewonnen!!!")
            }
            setMsg(data.winner + " hat gewonnen!!!")
        })
    }, [username, socket])
    if (loggedIn) {
        return (
            <div className="container d-flex flex-column justify-content-center h-100 my-2">

                <div className="d-flex flex-column justify-content-center mx-auto w-75">
                    <div className="d-flex align-items-center">
                        <h2 className="mx-auto">Hi {username}, herzlich wilkommen bei 4Gewinnt!!!</h2>
                        <button className="btn btn-danger w-25 btn-lg mx-auto my-4" onClick={() => logout()}>Logout</button>
                    </div>
                    {!gameState.running && (<button className={"btn btn-primary w-25 btn-lg mx-auto"} onClick={() => {
                        handleMatch()
                    }
                    }>Spiel finden</button>)}
                </div>
                <h3 className="mx-auto">{gameState.room}</h3>
                {(msg.length > 3) && <div className="mx-auto alert alert-success">{msg} </div>}
                {gameState.running && (<>
                    <div className="board p-2 mx-auto">
                        {gameState.board.map((row, index) => (
                            row.map((square, col) => {
                                return (<div key={index + col} onClick={e => turn(col)} style={{ backgroundColor: getColor(square) }} className="square m-1"></div>)
                            })
                        ))}
                    </div>
                    {gameState.running && <button className="btn btn-danger w-25 btn-lg mx-auto my-4" onClick={() => leave_room()} > Raum verlassen</button>}
                </>)
                }
            </div>
        )
    }
    return <Navigate to="/" replace />
}

export default GamePage