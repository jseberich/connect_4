import{io} from "socket.io-client"
import {createContext} from"react"

export const socket = io("http://localhost:8000",{transports: ["websocket", "polling"],query:"name=test"})

export const SocketContext = createContext()
 