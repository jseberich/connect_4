# Connect_4

Schulprojekt

## Voraussetzungen
- Nodejs (Version: >=14.15.1)
- Python (Version: >=3.10)
- das Python Modul virtualenv oder sonstige alternativen
## So startet man das Projekt:  
Nach dem Clonen des Repos:  
- In den Ordner "Backend/API" & "Backend/Multiplayer" jeweils ein Python Enviroment mit dem Befehl: "python -m venv env" erstellen und diese dann aktivieren
- Bei den beiden Enviroments die Requirements mit dem  Befehl "pip install -r requirements.txt" installieren
- Die Enviromentvariable DB setzten (der Wert ist der Connectionstring der Datenbank z.B "mysql://root@localhost:3306/test_db")
- Die Anwendung im API Ordner mit dem Befehl "uvicorn main:app --port=8001" starten
- Die Anwensung im Multiplayer Ordner mit dem Ordner mit dem Befehl "uvicorn server:app --port=8000" starten
- Um das UI zu starten installiert man die externen Module führt man im Ordner "frontend/connect_4/" den Befehl "npm install" aus
- Danach startet man dies mit dem Befehl "npm start"
