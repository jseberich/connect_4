from internal.db import Base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship


class UserModel(Base):
    __tablename__ = "Benutzer"
    id = Column("ID_Benutzer", Integer, primary_key=True, index=True)
    name = Column("Name", String(50))
    passwort = Column("Password", String(256))
