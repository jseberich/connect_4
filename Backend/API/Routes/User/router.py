from fastapi import APIRouter, HTTPException, Depends
from pydantic import BaseModel
from typing import Optional
from internal.db import get_db
from sqlalchemy.orm import Session
from .model import UserModel
from fastapi_jwt_auth import AuthJWT
from passlib.context import CryptContext

router = APIRouter(prefix="/user", tags=["Benutzer"], dependencies=[Depends(get_db)])


class UserPost(BaseModel):
    id: Optional[int]
    name: str
    passwort: str

    class Config():
        orm_mode = True


class UserResponse(BaseModel):
    id: int
    name: str

    class Config():
        orm_mode = True


pwd_context = CryptContext(schemes=["sha256_crypt"], deprecated="auto")


def hash_passwort(pw: str) -> str:
    return pwd_context.hash(pw)


@router.post("/register", response_model=UserResponse)
def register(p_user: UserPost, db: Session = Depends(get_db)):
    new_user = UserModel(name=p_user.name, passwort=hash_passwort(p_user.passwort))
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


@router.post("/login")
def login(p_user: UserPost, db: Session = Depends(get_db), auth: AuthJWT = Depends()):
    db_user = db.query(UserModel).filter_by(name=p_user.name).first()
    if pwd_context.verify(p_user.passwort, db_user.passwort):
        token = auth.create_access_token(subject=db_user.name)
        return {"jwt": token, "user": db_user}
    else:
        raise HTTPException(status_code=401, detail="Wrong Credentials")
