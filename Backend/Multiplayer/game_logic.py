import numpy as np


def create_board():
    return np.zeros((6,7))

class GameRunner():
    def __init__(self):
        
        self.game_over = False
        self.turn = 0
        self.rows, self.cols = 6, 7
        self.board = np.zeros((self.rows, self.cols))
    
    def drop_piece(self, row, col, piece) -> None:
        '''
        Ändert den State des Boards
        '''
        self.board[row][col] = piece
    
    def is_valid_column(self, col) -> bool:
        '''
        Prüft ob in der angegebenen Spalte ein Stein gesetzt werden kann
        '''
        return self.board[self.rows-1][col] == 0
    
    def get_first_open_row(self, col) -> int:
        '''
        returned den ersten freien platz in einer Spalte des Boards
        '''
        for r in range(self.rows):
            if self.board[r][col] == 0:
                return r
    
    def is_winning_move(self, piece) -> bool:
        '''
        Prüft ob das Spiel vorbei ist
        '''
        # Check horizontal
        for c in range(self.cols -3):
            for r in range(self.rows - 3):
               if self.board[r][c] == piece and self.board[r][c+1] == piece and self.board[r][c+2] == piece and self.board[r][c+3] == piece:
                    return True        
        # Check vertical
        for c in range(self.cols - 3):
            for r in range(self.rows - 3):
                if self.board[r][c] == piece and self.board[r+1][c] == piece and self.board[r+2][c] == piece and self.board[r+3][c] == piece:
                    return True
    
        # Check positively sloped diaganols
        for c in range(self.cols-3):
            for r in range(self.rows-3):
                if self.board[r][c] == piece and self.board[r+1][c+1] == piece and self.board[r+2][c+2] == piece and self.board[r+3][c+3] == piece:
                    return True
    
        # Check negatively sloped diaganols
        for c in range(self.cols-3):
            for r in range(3, self.rows):
                if self.board[r][c] == piece and self.board[r-1][c+1] == piece and self.board[r-2][c+2] == piece and self.board[r-3][c+3] == piece:
                    return True
        
        return False

    def step(self,col : int) -> None:
        '''
        Macht einen zug
        '''
        piece = 0
        if self.turn % 2 == 0:
            piece = 1
        else:
            piece = 2
        
        if self.is_valid_column(col):
            row = self.get_first_open_row(col)
            self.drop_piece(row, col, piece)
            if self.is_winning_move(piece):
                print("end")
                self.game_over = True
            self.turn += 1

    def reset(self) -> None:
        self.board =  np.zeros((self.rows, self.cols))
        self.turn = 0
        self.game_over = False
