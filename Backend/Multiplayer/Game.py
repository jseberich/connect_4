from game_logic import GameRunner
from EventManager import SIOManager
import numpy as np

class Game():
    def __init__(self, sid: str, sio):
        self.server = sio
        self.runner = GameRunner()
        self.room : str | None = None
        self.player_one = sid
        self.player_two = None
        self.cur_player = self.player_one

    def get_board(self) -> list[int]:
        return self.runner.board.tolist()
    
    async def turn(self, sid, turn) -> None:
        if not self.runner.game_over:
            if  self.cur_player == sid:
                self.runner.step(turn)
                await self.server.emit("new_board", {"board": np.flip(self.runner.board, 0).tolist()}, room=self.room)
            if self.runner.turn % 2 == 0 :
                self.cur_player = self.player_one
            else:
                self.cur_player = self.player_two

    async def reset_room(self) -> None:
        self.runner.reset()


