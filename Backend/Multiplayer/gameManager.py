from EventManager import SIOManager
from Game import Game

class GameManager:
    
    def __init__(self, server):
        self.games = []
        self.events = SIOManager(server)
        self.server = server

    def add_game(self, game: Game) -> None:
        self.games.append(game)

    def get_game_per_sid(self, sid: str) -> Game | None:
        for game in self.games:
            if game.player_one == sid or game.player_two == sid:
                return game
        return None

    async def match(self, sid: str) -> None:
        waiting: Game | None = None
        for game in self.games:
            if game.player_two == None:
                waiting = game
        if waiting is None:
            waiting = Game(sid, self.server)
            self.add_game(waiting)
            waiting.room = await self.events.create_room(sid)
            payload = {
                "room": waiting.room,
                "board": waiting.get_board()
            }
            await self.events.send_event("match", payload, waiting.room)
        else:
            await self.events.join_room(sid, waiting.room)
            waiting.player_two = sid
            payload = {
                "room" : waiting.room,
                "board" : waiting.get_board()
            }
            await self.events.send_event("match", payload, waiting.room)
        
    def get_player_number(self, game: Game, sid: str) -> int:
        game = self.get_game_per_sid(sid)
        if game.player_one == sid:
            return 1
        elif game.player_two == sid:
            return 2
    
    async def close_game(self, game: Game) -> None:
        self.server.leave_room(game.player_one, game)
        self.server.leave_room(game.player_two, game)
        self.games.remove(game)

    async def handle_disconnect(self, sid: str) -> None:
        game = self.get_game_per_sid(sid)
        if game is not None:
            number = self.get_player_number(game, sid)
            if number == 1:
                await self.close_game(game)
            elif number == 2:
                self.server.leave_room(sid)
                game.player_two = None