from ast import Str
import socketio
from Game import Game
from EventManager import SIOManager
from gameManager import GameManager
sio = socketio.AsyncServer(async_mode = "asgi", cors_allowed_origins='*')

app = socketio.ASGIApp(sio)

events = SIOManager(sio)
manager = GameManager(sio)


@sio.event
def connect(sid, environ, auth):
    print("Connect: ", sid, auth)


@sio.event
async def disconnect(sid: Str):
    await manager.handle_disconnect(sid)     


@sio.event
async def message(sid : str, data):
    await sio.save_session(sid, {"username": data})


@sio.event
async def match(sid: str):
    await manager.match(sid)


@sio.event
async def leave_room(sid: str):
    try:
        game = manager.get_game_per_sid(sid)
        number = manager.get_player_number(game, sid)
        if number == 1:
            await manager.close_game(game)
        else:
            manager.leave_room(sid)
            await events.send_event("leave_room", room=game.player_one)

    except Exception as e:
        print(e)


@sio.event
async def turn(sid: str, data: dict):
        game = manager.get_game_per_sid(sid)
        await game.turn(sid, data["data"])
        
        if game.runner.game_over:
            payload = {
                "winner": await events.get_username(sid)
            }
            await events.send_event("end", payload=payload, room=game.room)
            await manager.close_game(game)


@sio.event
async def reset_room(sid: str):
    try:
        game = manager.get_game_per_sid(sid)
        game.reset_room()
    except Exception as e:
        print(e)

