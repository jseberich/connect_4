FROM python:3.9.10

WORKDIR /usr/src/app

COPY requiremnts.txt ./

RUN pip install --no-cache-dir -r requiremnts.txt

COPY . ./

EXPOSE 8000

CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "8000"]