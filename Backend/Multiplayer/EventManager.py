from tkinter.simpledialog import SimpleDialog
from typing import Optional


class SIOManager:

    def __init__(self, server):
        self.server = server

    async def get_username(self, sid) -> str:
        async with self.server.session(sid) as session:
            return session["username"]

    async def create_room(self, sid: str) -> str:
        name = await self.get_username(sid)
        room = f"{name} room"
        self.server.enter_room(sid, room)
        return room

    async def join_room(self, sid, room : str) -> None:
        self.server.enter_room(sid, room)

    async def send_event(self, event_type: str, payload: Optional[dict], room: str) -> None:
        await self.server.emit(event_type, payload, room= room)

